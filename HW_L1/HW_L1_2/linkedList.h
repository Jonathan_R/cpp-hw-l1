#ifndef LINKEDL_H
#define LINKEDL_H

typedef struct node
{
	int value;
	struct node* next;

}node;

typedef struct head
{
	int size;
	struct node* next;

}head;

void initList(head* h); 
void cleanList(head* h);
void addNode(head* h, int value); 
void removeNode(head* h, int index);
int getNode(head* h, int index); //return -1 if the index is invalid

#endif