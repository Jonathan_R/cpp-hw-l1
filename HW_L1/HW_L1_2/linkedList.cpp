#include <iostream>;
#include "linkedList.h";


void initList(head* h)
{
	//initializing the head
	h->size = 0;
	h->next = nullptr;
}

void cleanList(head* h)
{
	//creating variables (current and next)
	node* curr = h->next;
	node* next;

	//looping and deleting everything by saving the next pointer after current and deleting current 
	while (curr != nullptr && h->size!=0)
	{
		next = curr->next;
		delete curr;
		curr = next;
	}

	//deleting the head
	delete h;
}

void addNode(head* h, int value)
{
	node* curr = h->next;

	//checking if the list is empty
	if (h->size > 0)
	{
		//looping in the list
		for (int i = 0; i < h->size - 1; i++)
		{
			curr = curr->next;
		}

		//increasing the size and adding the new node
		curr->next = new node;
		curr->next->next = nullptr;
		curr->next->value = value;

		h->size++;
	}
	else
	{
		//adding the node and increasing the size
		h->next = new node;
		h->next->next = nullptr;
		h->next->value = value;

		h->size++;
	}
}
void removeNode(head* h, int index)
{
	node* curr = h->next;
	node* prev = curr;
	node* temp;

	//checking if the index is legal
	if (index <= h->size && index >= 0)
	{	
		//searching for the index node
		for (int i = 1; i < index; i++)
		{
			prev = curr;
			curr = curr->next;
		}

		//checking if its the last node
		if (index == h->size)
		{
			if (index != 1)
			{
				prev->next = nullptr;
			}
			else
			{
				h->next = nullptr;
			}

			delete curr;
		}
		else
		{
			if (index != 1)
			{
				prev->next = curr->next;
			}
			else
			{
				h->next = curr->next;
			}

			delete curr;
		}

		//decreasing the list size
		h->size--;
		
	}
}

int getNode(head* h, int index)
{
	node* curr = h->next;

	if (index <= h->size && index >= 0)
	{
		//looping in the list to get the index node
		for (int i = 1; i < index; i++)
		{
			curr = curr->next;
		}

		//returning the value of the requested index
		return curr->value;
	}
	else
	{
		return -1;
	}
}