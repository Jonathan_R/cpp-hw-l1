#include <iostream>;
#include "stack.h";
#include "utils.h";

using namespace std;

void reverse(int* nums, unsigned int size)
{
	//creating the stack
	stack* numbers = new stack;

	//initializing the stack
	initStack(numbers);

	//coping the numbers for the array to the stack
	for (int i = 0; i < size; i++)
	{
		push(numbers, nums[i]);
	}

	//poping out the numbers and storing in the array which will save them in backwards order
	for (int i = 0; i < size; i++)
	{
		nums[i] = pop(numbers);
	}

	//cleaning the stack
	cleanStack(numbers);
}

int* reverse10()
{
	//creating the array and the input buffer
	int* arr = new int[10];
	int num = 0;

	//looping in the array
	for (int i = 0; i < 10; i++)
	{
		//getting the numbers from the user and storing them in the array
		cout << "Enter number " << i+1 <<": ";
		cin >> num;
		cout << endl;

		arr[i] = num;
	}

	//reversing the array
	reverse(arr, 10);

	return arr;
}