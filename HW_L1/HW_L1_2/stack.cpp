#include <iostream>;
#include "stack.h";

void initStack(stack* s)
{
	//initializing the stack linked list head
	s->h = new head;
	initList(s->h);
}

void cleanStack(stack* s)
{
	//cleaning the stack linked list head
	cleanList(s->h);
}

void push(stack* s, unsigned int element)
{
	//adding new element to the stack using the linked list add node function
	addNode(s->h, element);
}

int pop(stack* s)
{
	//getting the requested value
	int value = getNode(s->h, s->h->size);
	//deleting the element
	removeNode(s->h, s->h->size);

	return value;
}