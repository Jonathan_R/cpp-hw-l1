#include "queue.h";
#include <stdio.h>;
#include <stdlib.h>;
#include <iostream>

using namespace std;

void initQueue(queue* q, unsigned int size)
{
	//initializing the array, the size and the counter
	q->arr = new int[size];
	q->size = size;
	q->count = 0;

	//initializing the array with -1 as blank space
	for (int i = 0; i < q->size; i++)
	{
		q->arr[i] = -1;
	}
}

void cleanQueue(queue* q)
{
	//deleting the array from the ram
	delete[] q->arr;
}

void enqueue(queue* q, unsigned int newValue)
{
	//checking for entering overflow using the counter
	if (q->count < q->size)
	{
		//if there is a blank space then this adding new value to the array
		q->arr[q->count] = newValue;
		//increasing the counter
		q->count++;
	}
}

int dequeue(queue* q)
{
	//creating temporary var
	int temp = 0;

	//checking if the queue is empty
	if (q->count!=0)
	{
		//saving the first value for return
		temp = q->arr[0];

		//shifting the values index down
		for (int i = 1; i < q->size; i++)
		{
			q->arr[i - 1] = q->arr[i];
		}

		
		q->arr[q->size - 1] = -1;

		//decreasing the counter
		q->count--;

		return temp;
	}
	else
	{
		return -1;
	}
}